﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimplonBank.Models
{
// Compte épargne
    public class SavingAccount : Account
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public float RateOfInterest { get; set; }

        public float MinimumAmount { get; set; }

        void DebitTheAccount(float amount)
        {
            if ( amount >  BalanceOfAccount / 3 || amount>100)
            {
                Console.WriteLine("Retrait non autorisé");
            }
            else
            {
                this.BalanceOfAccount -=amount;
            }
                               
        }

         void CreditTheAccount(float amount)
        {
            this.BalanceOfAccount += amount;
        }


    }
}
