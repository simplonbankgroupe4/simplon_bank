﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace SimplonBank.Models
{
    public class CheckingAccount : Account
    {
        public string Name { get; set; } = "Compte courant";

        public int Overdraft { get; set; } = -200;

        public CheckingAccount(int id, float balanceOfAccount, DateTime dateOfCreation, string accountNumber)
            :base(id,balanceOfAccount,dateOfCreation,accountNumber)
        {
            
        }

        //Méthode permettant de réaliser un virement vers le compte d'un autre client
        public void InterClientTransfer(float amount, CheckingAccount receveur)
        {
            if (amount == 0)
            {
                Console.WriteLine("Aucun montant indiqué.");
            }

            if (BalanceOfAccount - amount < Overdraft)
            {
                Console.WriteLine("Virement impossible !");
            }
            else
            {
                DebitTheAccount(amount);
                receveur.CreditTheAccount(amount);
                Console.WriteLine("Virement effectué avec succès !");
            }
        }
        
        public void afficher(){
            Console.WriteLine(BalanceOfAccount+ " euros");
        }
    }
}

