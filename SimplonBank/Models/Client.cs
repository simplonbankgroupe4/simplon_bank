﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimplonBank.Models
{
    public class Client
    {
        public int id { get; set; }

        public string firstname { get; set; }

        public string lastname { get; set; }

        public int amount { get; set; }
    }
}
