﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimplonBank.Models;

    public abstract class Account
    {
        protected int Id { get; }

        protected float BalanceOfAccount { get; set; }

        protected DateTime DateOfCreation { get; set; }

        protected string AccountNumber { get; set; }

        public Account(int id, float balanceOfAccount, DateTime dateOfCreation, string accountNumber)
        {
        Id = id;
        BalanceOfAccount = balanceOfAccount;
        DateOfCreation = dateOfCreation;
        AccountNumber = accountNumber;
        }
        public  void DebitTheAccount(float Amount)
        {
            this.BalanceOfAccount -= Amount;


        }

        protected void CreditTheAccount(float Amount)
        {
            this.BalanceOfAccount += Amount;
        }
        public override string ToString()
        {
        return $"Le solde est de {BalanceOfAccount}";
        }


}





