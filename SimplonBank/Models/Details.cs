﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimplonBank.Models
{
    public class Details
    {
        public int Id { get; set; }

        public string TrackType { get; set; }

        public string Name { get; set; }

        public string ZipCode { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }
    }
}
