﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimplonBank.Models
{
    public class CreditCard
    {
        public int Id { get; set; }

        public string firstname { get; set; }

        public string lastname { get; set; }

        public string number { get; set; }

        public string cvv { get; set; }

        public DateTime ExpirationDate { get; set; }
    }
}
