﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimplonBank.Models
{
    public class Bank
    {
        public int Id { get; set; }

        public string Name { get; set; }
        

        public List<Client> Clients{ get; set; }

        public override string ToString()
        {
            var clientsToString = new StringBuilder();
            Clients?.ForEach(client =>
            {
                clientsToString.AppendLine(client.ToString());
            });

            return $"Prénom: {clientsToString}";
        }

        string InformTheCustomer()
        {
            return "Une nouvelle promotion pour nos anciens clients !";
        }

        void OpenAnAccount(string client)
        {
            if (new Client().amount < 300)
            {
                throw new Exception("Vous n'avez pas suffisement d'argent.");
            }
        }
    }
}
